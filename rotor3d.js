/**
 * rotor3d.js
 */
/*========================================================================================*/
var Glob = {
    static_count : 0, /* Number of currently created components */
    button_id : "MyButton",
    canvas_id : "MyCanvas",
    canvas_width : 800,
    canvas_height : 800,
    background_color : "#000",
    rotor_color : "#000", //"#ff4d4d",
    rotor_blade_length : 340,
    rotor_blade_thickness : 50,
    rotor_diff_angle : 5,
    fps : 1,
};

/*========================================================================================*/
class Point {
    constructor (x,y) {
        this.x = x;
        this.y = y;
    }
}

class Punkt extends Point {
    constructor (x,y) {
        super(x,y);
    }
}

/*========================================================================================*/
var Origin = function () {
    var MathPIHalf = Math.PI / 2; // 90°
    var MathPIDouble = Math.PI * 2; // 360°
    this.canvas = document.getElementById(Glob.canvas_id);
    this.context = this.canvas.getContext('2d');
    this.ConvertDegreeToRadian = function (deg) {
        return (deg / 180 * Math.PI);
    };
    this.GetEndPointOfLine = function (startX, startY, /* radian */ angle, length, clockwise = false) {
        if (angle > MathPIDouble) angle -= MathPIDouble;
        var Dx = 0, Dy = 0;
        var _endX = 0;
        var _endY = 0;
        // Calculate angle and end points
        if (angle < MathPIHalf) { // < 90°
            Dx = length * Math.cos(angle);
            Dy = length * Math.sin(angle);
            _endX = startX + Dx;
            _endY = (clockwise) ? (startY + Dy) : (startY - Dy);
        }
        else if (angle == MathPIHalf) {
            _endX = startX;
            _endY = (clockwise) ? (startY + length) : (startY - length);
        }
        else if (angle > MathPIHalf && angle < Math.PI) {
            Dx = length * Math.cos(Math.PI - angle);
            Dy = length * Math.sin(Math.PI - angle);
            _endX = startX - Dx;
            _endY = (clockwise) ? (startY + Dy) : (startY - Dy);
        }
        else if (angle == Math.PI) {
            _endX = startX - length;
            _endY = startY;
        }
        else if (angle > Math.PI && angle < (3 * MathPIHalf)) {
            Dx = length * Math.cos(angle - Math.PI);
            Dy = length * Math.sin(angle - Math.PI);
            _endX = startX - Dx;
            _endY = (clockwise) ? (startY - Dy) : (startY + Dy);
        }
        else if (angle == (3 * MathPIHalf)) {
            _endX = startX;
            _endY = (clockwise) ? (startY - length) : (startY + length);
        }
        else if (angle > (3 * MathPIHalf) && angle < MathPIDouble) {
            Dx = length * Math.cos(MathPIDouble - angle);
            Dy = length * Math.sin(MathPIDouble - angle);
            _endX = startX + Dx;
            _endY = (clockwise) ? (startY - Dy) : (startY + Dy);
        }
        else if (angle == MathPIDouble || angle == 0) {
            _endX = startX + length;
            _endY = startY;
        }
        return { x : _endX, y : _endY };
    };
    /*
     *
     *            1/2 * PI
     *                |
     *                |
     *     PI ________o_________ 0
     *                |         (2 PI)
     *                |
     * 
     *            3/2 * PI
     * 
     * Drawing process starts by 0 and continues in clockwise/anticlockwise directorion.
     *
     */
    this.DrawLineByAngle = function (startX, startY, /* radian */ angle, length, clockwise = false) {    
        this.context.beginPath();
        this.context.moveTo(startX, startY);
        var _EndPoint = this.GetEndPointOfLine(startX, startY, angle, length, clockwise);
        this.context.lineTo(_EndPoint.x, _EndPoint.y);
        this.context.closePath();
        this.context.stroke();
    };
    this.FillRectByAngle = function (startX, startY, /* radian */ angle, length, thickness, thickness_clockwise = true, clockwise = false) {
        this.context.beginPath();
        this.context.moveTo(startX, startY);
        var _EndPoint = this.GetEndPointOfLine(startX, startY, angle, length, clockwise);
        this.context.lineTo(_EndPoint.x, _EndPoint.y);
        // Determine points for thickness
        _EndPoint = this.GetEndPointOfLine(_EndPoint.x, _EndPoint.y, thickness_clockwise ? (MathPIHalf - angle) : (MathPIHalf + angle), thickness, thickness_clockwise);
        this.context.lineTo(_EndPoint.x, _EndPoint.y);
        _EndPoint = this.GetEndPointOfLine(startX, startY, thickness_clockwise ? (MathPIHalf - angle) : (MathPIHalf + angle), thickness, thickness_clockwise);
        this.context.lineTo(_EndPoint.x, _EndPoint.y);
        // Close polygon path and stroke/fill
        this.context.closePath();
        this.context.fill();
    };
};

/*========================================================================================*/
class Background extends Origin {
    constructor () {
        super(); // Origin.call(this); // inherits from Origin
        this._id = ++Glob.static_count;
    }
    /* Methods */
    Paint () {
        this.context.fillStyle = Glob.background_color;
        this.context.fillRect(0,0, this.canvas.width, this.canvas.height);
    }
    Init () {
        this.canvas.width = Glob.canvas_width;
        this.canvas.height = Glob.canvas_height;
        this.Paint();
    }
    /* Properties */
    get id() {
        return this._id;
    }
}

/*========================================================================================*/
/*var Component = function (name) {
    Origin.call(this); // inherits from Origin
    this.name = name;
    this.SetPosition = function (x, y) {
        this.x = x;
        this.y = y;
    };
};*/

class Component extends Origin {
    constructor(name) {
        super(); // Origin.call(this); // inherits from Origin
        this._id = ++Glob.static_count;
        this._name = (name === undefined) ? "" : name;
    }
    /* Methods */
    GetName() {
        return this._name;
    }
    SetPosition(x,y) {
        this.x = x;
        this.y = y;
    }
    /* Properties */
    get id() {
        return this._id;
    }
    get name() {
        return this._name;
    }
}

/*========================================================================================*/
class Rotor extends Component {
    constructor(name) {
        super(name);
        this._thickness = Glob.rotor_blade_thickness;
        this._length = Glob.rotor_blade_length; // length of one blade
        this._angle = 0; // Degree        
    }
    /* Methods */
    Paint () {
        this.context.fillStyle = Glob.rotor_color;
        // Draw right rotor blade
        this.FillRectByAngle(this.x, this.y, this.ConvertDegreeToRadian(this._angle), this._length, this._thickness);
        // Draw left rotor blade
        //this.context.fillStyle = "green";
        this.FillRectByAngle(this.x, this.y, this.ConvertDegreeToRadian(this._angle + 180), this._length, this._thickness, false);
        // Coloring one point
        var _Endpoint = this.GetEndPointOfLine(this.x, this.y, this.ConvertDegreeToRadian(this._angle), this._length / 2);
        var _Endpoint2 = this.GetEndPointOfLine(this.x, this.y, this.ConvertDegreeToRadian(this._angle), this._length / 4);
        this.context.fillStyle = "yellow";
        this.context.beginPath();
        this.context.arc(_Endpoint.x, _Endpoint.y, 6, 0, Math.PI * 2);
        this.context.arc(_Endpoint2.x, _Endpoint2.y, 6, 0, Math.PI * 2);
        this.context.fill();
    }
    Init (x,y) {
        if ( x === undefined && y === undefined ) {
            this.x = this.canvas.width / 2;
            this.y = this.canvas.height / 2;
        }
        else {
            this.SetPosition(x,y);
        }
        this.Paint();
    }
    Update () {
        this.context.fillStyle = Glob.background_color;
        this.context.fillRect(this.x - this._length - 10, this.y - this._length - 10, 2 * this._length + 20, 2 * this._length + 20);
        this.Paint();
    }
    Rotate (diffAngle) { // Aktueller Winkel w. Rotiert um einen Winkel dw  =>  Endwinkel (w + dw).
        this._angle += diffAngle;
        if (this._angle >= 360) this._angle -= 360;
        this.Update();
    }
    /* Properties */
    get angle() {
        return this._angle; // Degree
    }
    get thickness() {
        return this._thickness;
    }
}

/*========================================================================================*/
class Application extends Component {
    constructor(name) {
        super(name);
        this.isRunning = false;
        this.animId = null; // Animation Id
    }
    Init () {
        this.button = document.getElementById(Glob.button_id);
        this.background = new Background();
        this.rotor = new Rotor("Rotor");
        this.background.Init();
        this.rotor.Init();
    }
    Start () {
        if (this.rotor === undefined || this.rotor == null) return;
        let _App = this;
        _App.button.addEventListener ("click", function () {
            if (!_App.isRunning) {
                this.innerHTML = "Stop";
                _App.animId = window.setInterval(function () {
                    _App.rotor.Rotate(Glob.rotor_diff_angle);
                }, Glob.fps);
            }
            else {
                this.innerHTML = "Start";
                _App.Stop();
            }
            _App.isRunning = !_App.isRunning;
        }, false);
    }
    Stop () {
        window.clearInterval(this.animId);
    }
}


/*========================================================================================*/
let MyApp = new Application("Rotor Simulation");
MyApp.Init();
MyApp.Start();

